clear all;
close all;
clc;

pkg load pythonic;

pyversion
py.sys.path.insert(int32(0), './');

DATABASE = 3;
RXGRP = 4; % Receiver group [2 - Belt, 4 - Upwards, 5 - Diagonal, 6 - Head]
scenario_name = 'Sitting';
#scenario_name = 'Standing';

BEAMS = 15;
RESOL = pi/BEAMS;

ANG_EL = [-pi:RESOL*2:pi];
ANG_AZ = [-pi:RESOL:pi];

[GR_EL, GR_AZ] = meshgrid(ANG_EL, ANG_AZ);

ANT_GAIN = abs(sinc(GR_AZ) .* sinc(GR_EL));

NCOMP = 5; % Number of components to plot
cm = cool;
switch DATABASE
    case 4
        %UNCLEAR
        poly_scenario_name = 'Human_sitting_legsback';
        folder_name = strcat('../Grid_antenna/');
        tail = 't%03d_01.r%03d.p2m';
    case 1
        %scenario_name = 'Standing';
        poly_scenario_name = 'Human_sitting_legsback';
        %RXGRP = 4; % Receiver group [2 - Belt, 3 - Upwards, 5 - Diagonal, 6 - Head]
        %folder_name = strcat('OLD/',scenario_name, '/');
        folder_name = strcat(scenario_name, '/Detailed/');
        tail = 't%03d_01.r%03d.p2m';scenario_name = 'sitting';

    case 2 %WORKS
        poly_scenario_name = 'Human_sitting_legsback';
        folder_name = strcat(scenario_name, '/Lowpoly/');
        tail = 't%03d_01.r%03d.p2m';
    case 3 % THE ONE!!
        switch scenario_name
            case 'Sitting'
                poly_scenario_name = 'HMS_lp2_sitting';
            case 'Standing'
                poly_scenario_name = 'HMS_lp2';
            otherwise
                display('Error: unknown scenario!');
            return
        end
        tail = 't%03d_01.r%03d.p2m';
        if RXGRP == 4
            RXGRP = 3;
        end
        folder_name = strcat('OLD/',scenario_name, '/');
    otherwise
        display('Error: unknown scenario!');
        return
end
file_name_example = strcat(folder_name,sprintf(strcat(poly_scenario_name, '.xxx.', 't%03d_01.r%03d.p2m'), 1, RXGRP));
display(strcat('I will read <', file_name_example,'>'))
%scenario_name = 'Standing';poly_scenario_name = 'Human_sitting_legsback';
%poly_scenario_name = 'Human_crawl';




%% READ DATA
data =  'cir';
[data_rx2, data_comp2, data_pwr2, data_phs2, data_delay2, temp1, temp2, error1] = ...
    Read_WI_file(folder_name, poly_scenario_name, tail, 'cir', RXGRP, NCOMP);
[data_rx2_dod, data_comp2_dod, data_pwr2_dod, temp4, temp3, data_az2_dod, data_el2_dod, error2] = ...
    Read_WI_file(folder_name, poly_scenario_name, tail, 'dod', RXGRP, NCOMP);     
[data_rx2_doa, data_comp2_doa, data_pwr2_doa, temp4, temp3, data_az2_doa, data_el2_doa, error3] = ...
    Read_WI_file(folder_name, poly_scenario_name, tail, 'doa', RXGRP, NCOMP);      
if error1 || error2 || error3
    display('Error: cannot open the file!');
    return
end
if    ~isequaln(data_rx2_dod,data_rx2_doa) || ~isequaln(data_rx2,data_rx2_dod)...
        ~isequaln(data_comp2,data_comp2_doa) || ~isequaln(data_comp2,data_comp2_dod)
        display('Error: different data sets!');
        return
end
if    ~isequaln(data_pwr2,data_pwr2_doa) || ~isequaln(data_pwr2,data_pwr2_dod)
        display('Error: different data sets!');
        return
end


color_scheme(1,1:3) = [0 0 0]; color_scheme2(1,1:3) = [0 0 0];
color_max = [1 0.5 1];
delta_color = (color_max - color_scheme(1,1:3))/(NCOMP - 1);
delta_color2 = (color_max - color_scheme2(1,1:3))/(size(data_pwr2,2) - 1);
for i = 2:size(data_pwr2,1)
    color_scheme(i,1:3) = color_scheme(i-1,1:3) + delta_color;
end
for i = 2:size(data_pwr2,2)
    color_scheme2(i,1:3) = color_scheme2(i-1,1:3) + delta_color2;
end



INDIVIDUAL_PLOTS = 1;

if INDIVIDUAL_PLOTS
  PHASE_PLOT = 1;
  if PHASE_PLOT
      figure(1); hold on; grid on;
      plot(1:size(data_phs2,1), data_phs2', '*')
      xlabel('RX index');
      ylabel('Phase [deg]');
      xlim([0,120]);
      print(sprintf("Phase.tex"), "-dtex", "-F:26");


      %figure(2); hold on; grid on;
      %xlabel('RX index');
      %ylabel('Phase [deg]');
      %for i = 1:size(data_comp2,1)
      %    plot(1:size(data_phs2,2), data_phs2(i,:), '*', 'Color',   min([1 1 1],color_scheme(i,1:3)));
      %end
      %figure(3); hold on; 
      %for i = 1:size(data_comt p2,2)
      %    plot(1:size(data_phs2,1), data_phs2(:,i), '*', 'Color',  min([1 1 1],color_scheme2(i,1:3)));
      %end

      %figure(4); hist3([reshape(data_phs2, 1, size(data_phs2,1)*size(data_phs2,2));...
      %    reshape(data_comp2, 1, size(data_comp2,1)*size(data_comp2,2))]');
      
      %figure(5); hold on; grid on;
      %title('Simulation')
      %xlabel('RX index');
      %ylabel('Phase [deg]');
      %for i = 1:size(data_comp2,2)
      %    for j = 1:size(data_comp2,1)
      %         data_rx2_sim(i,j) = j;
      %         data_comp2_sim(i,j) = i;
      %         data_phs2_sim(i,j) = rand*360-180;
      %    end
      %    plot(1:size(data_phs2,1), data_phs2_sim(i,:), '*', 'Color',  min([1 1 1],color_scheme2(i,1:3)));
      %send


      %[f,x] = ecdf(reshape(data_phs2, 1, size(data_phs2,1)*size(data_phs2,2)));
      %figure; grid on; ecdfhist(f,x,10);
      %ylabel('PDF');
      %xlabel('Phase [deg]');
  end
  
  
  POWER_PLOT = 1;
  if POWER_PLOT
      figure(5); hold on; grid on;
      plot(1:size(data_pwr2,1), data_pwr2', '*')
      xlabel('RX index');
      ylabel('Received power [dBm]');
      xlim([0,120]);
      grid on;
      legend("1st comp.", "MPC-1", "MPC-2", "MPC-3", "MPC-4", "location", "eastoutside");
      #set (findobj (gcf, "-property", "interpreter"), "interpreter", "latex");
      print(sprintf("Power_up.tex"), "-dtex", "-F:24");
  end
  
    DELAY_PLOT = 1;
    if DELAY_PLOT
       figure(6); hold on; grid on;
       plot(1:size(data_delay2,1), 1e9 * data_delay2', '*')
       xlabel('RX index');
       ylabel('Delay [ns]');
       grid on;
       xlim([0,120]);
       print("Delay.tex", "-dtex", "-F:26");
    end
  
  
    ANGLE_PLOT = 1;
    if ANGLE_PLOT
        figure(7); hold on; grid on;
        plot(1:size(data_el2_doa,1), data_el2_doa', '*')
        xlabel('RX index');
        ylabel('Elevation [deg]');
        xlim([0,120]);
        print("Elevation_DOA.tex", "-dtex", "-F:26");

        figure(8); hold on; grid on;
        plot(1:size(data_az2_doa,1), data_az2_doa', '*')
        xlabel('RX index');     
        ylabel('Azimuth [deg]');
        xlim([0,120]);
        print("Azimuth_DOA.tex", "-dtex", "-F:26");


        figure(9); hold on; grid on;
        plot(1:size(data_el2_dod,1), data_el2_dod', '*')
        xlabel('RX index');
        ylabel('Elevation [deg]');xlim([0,120]);
        print("Elevation_DOD.tex", "-dtex", "-F:26");

        figure(10); hold on; grid on;
        plot(1:size(data_az2_dod,1), data_az2_dod', '*')
        xlabel('RX index');     
        ylabel('Azimuth [deg]');
        xlim([0,120]);
        print("Azimuth_DOD.tex", "-dtex", "-F:26");
        
        figure(11); hold on; grid on;
        plot(1:size(data_pwr2_doa,1), data_pwr2_doa', '*')
        xlabel('RX index');     
        ylabel('Power [dBm]');
        xlim([0,120]);
        print("Power.tex", "-dtex", "-F:26");
    end
    
      PHASE_PLOT = 0;
  if PHASE_PLOT
      figure(12); hold on; grid on;
      plot(1:size(data_phs2,1), data_phs2', '*')
      xlabel('RX index');
      ylabel('Phase [deg]');
      grid on;
      #legend("!st component", "MPC-1", "MPC-2", "MPC-3", "MPC-4", "location", "westoutside");
      print("Phase.svg", "-dsvg");
  end
end
 