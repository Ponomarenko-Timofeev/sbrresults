clear all;
clc;

pkg load miscellaneous;

DATABASE = 3;
colors = {[217/255, 83/255, 25/255], [0, 144/255, 1.0]};
for RXGRP = [2,4,5] % Receiver group [2 - Belt, 4 - Upwards, 5 - Diagonal, 6 - Head]
  %scenario_name = 'Sitting';
  scenario_name = 'Standing';

  NCOMP = 10; % Number of components to plot
  cm = copper;
  switch scenario_name
    case 'Sitting'
      poly_scenario_name = 'HMS_lp2_sitting';
    case 'Standing'
      poly_scenario_name = 'HMS_lp2';
    otherwise
      display('Error: unknown scenario!');
      return
    end
    tail = 't%03d_01.r%03d.p2m';
    if RXGRP == 4
      RXGRP = 3;
    end
    folder_name = strcat('OLD/',scenario_name, '/');

  file_name_example = strcat(folder_name,sprintf(strcat(poly_scenario_name, '.xxx.', 't%03d_01.r%03d.p2m'), 1, RXGRP));
  display(strcat('I will read <', file_name_example,'>'))
%scenario_name = 'Standing';poly_scenario_name = 'Human_sitting_legsback';
%poly_scenario_name = 'Human_crawl';

%% READ DATA
  data =  'cir';
  [data_rx2, data_comp2, data_pwr2, data_phs2, data_delay2, temp1, temp2, error1] = ...
      Read_WI_file(folder_name, poly_scenario_name, tail, 'cir', RXGRP, NCOMP);
  [data_rx2_dod, data_comp2_dod, data_pwr2_dod, temp4, temp3, data_az2_dod, data_el2_dod, error2] = ...
      Read_WI_file(folder_name, poly_scenario_name, tail, 'dod', RXGRP, NCOMP);     
  [data_rx2_doa, data_comp2_doa, data_pwr2_doa, temp4, temp3, data_az2_doa, data_el2_doa, error3] = ...
      Read_WI_file(folder_name, poly_scenario_name, tail, 'doa', RXGRP, NCOMP);      

  if error1 || error2 || error3
    display('Error: cannot open the file!');
    return
  end

  
  if    ~isequaln(data_rx2_dod,data_rx2_doa) || ~isequaln(data_rx2,data_rx2_dod)...
        ~isequaln(data_comp2,data_comp2_doa) || ~isequaln(data_comp2,data_comp2_dod)
        display('Error: different data sets!');
        return
  end

  if    ~isequaln(data_pwr2,data_pwr2_doa) || ~isequaln(data_pwr2,data_pwr2_dod)
        display('Error: different data sets!');
        return
  end


  color_scheme(1,1:3) = [0 0 0]; color_scheme2(1,1:3) = [0 0 0];
  color_max = [1 0.5 1];
  delta_color = (color_max - color_scheme(1,1:3))/(NCOMP - 1);
  delta_color2 = (color_max - color_scheme2(1,1:3))/(size(data_pwr2,2) - 1);

  for i = 2:size(data_pwr2,1)
    color_scheme(i,1:3) = color_scheme(i-1,1:3) + delta_color;
end
for i = 2:size(data_pwr2,2)
    color_scheme2(i,1:3) = color_scheme2(i-1,1:3) + delta_color2;
end

nc = [5,NCOMP];

figure(RXGRP);
hold on;

d = data_delay2(1,1:i) * 3e8;
dist1 = d(1);

dists = [dist1:5e-3:dist1 + (size(data_pwr2,1) -1) * 5e-3];

  switch scenario_name
    case 'Sitting'
      plot(dists', data_pwr2(:,1), 'k-', 'linewidth', 2);
    case 'Standing'
      plot(dists', data_pwr2(:,1), 'k--', 'linewidth', 2);
    otherwise
      display('Error: unknown scenario!');
      return
    end

disp("Calculating received powers...");
for i = nc
  disp(sprintf("For %d components...", i))
  P = [];
  dists = [];

  fftwf = [];
  for k = 1:1:length(data_delay2)
    text_waitbar( (find(nc==i) * length(data_delay2) + k) / (length(nc) * length(data_delay2)) );
    d = data_delay2(k,1:i);
    if k == 1
      dist1 = d(1) * 3e8;
    endif

    p = data_pwr2(k,1:i);
    ph = data_phs2(k,1:i);
    [PL, PLr, gridd, fftgrid] = CSI_sample(p, d, 80e6, 60e9, ph);
    P = [P, PL];
    dists = [dists, dist1 + 5e-3 * (k - 1)];
    fftwf = [fftwf; fftgrid];
  end



  switch scenario_name
    case 'Sitting'
      plot(dists, P, 'color', cell2mat(colors(1, i/5)), 'linestyle', '-', 'linewidth', 2);
    case 'Standing'
      plot(dists, P, 'color', cell2mat(colors(1, i/5)), 'linestyle', '--', 'linewidth', 2);
  end
end
ylim([-100, -40]);
xlim([0, max(dists)]);
legend('1st Component', 'Summary of 5', 'Summary of 10', "location", "southwest");
grid on;
box on;
ylabel("Received power [dBm]");
xlabel("RX/TX separation [m]");
print(sprintf("Rpow_%d.svg", RXGRP), '-dsvg', "-FArial:24");
hold off;
end
