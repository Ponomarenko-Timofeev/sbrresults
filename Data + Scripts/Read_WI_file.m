function [data_rx2, data_comp2, data_pwr2, data_phs2, data_delay2, data_az2, data_el2, error] = Read_WI_file(folder_name, poly_scenario_name, tail, data, RXGRP, NCOMP)
data_rx2 = [];
data_phs2 = [];
data_delay2 = [];
data_comp2 = [];
data_az2 = [];
data_el2 = [];
data_pwr2 = [];
error = 0;
    
file_name_example = strcat(folder_name,sprintf(strcat(poly_scenario_name, '.', data, '.', 't%03d_01.r%03d.p2m'), 1, RXGRP));
fhandle = fopen(file_name_example);
if fhandle == -1
    display('Error: cannot open the file!');
    error = 1;
    return
end

if data == 'cir'
    fgetl(fhandle);
    fgetl(fhandle);
else
    fgetl(fhandle);
end
all_points = str2num(fgetl(fhandle))
numpoints = all_points(1);

dataX = [];
dataC = [];

if data == 'cir'
	data_pwr = [];
    data_phs = [];
    data_delay = [];
    data_comp = [];
else
    data_az = [];
    data_el = [];
    data_pwr = [];
    data_comp = [];
end




if data == 'cir' 
    for i = 1:numpoints
        line = str2num(fgetl(fhandle));
        rxi = line(1); numcomps2 = line(2); %counts  = line(3); errmsg = line(4);
        for j = 1:numcomps2
            line = str2num(fgetl(fhandle));
            compi = line(1); phs = line(2); delay = line(3); pwr = line(4); 

            if j > NCOMP
                continue;
            end
            data_rx2(i,j) = rxi;
            data_comp2(i,j) = compi;        
                data_phs2(i,j) = phs;
                data_pwr2(i,j) = pwr;
                data_delay2(i,j) = delay;     
        end
    end
    NaN_index = find(data_phs2 == 0);
else
    for i = 1:numpoints
        line = str2num(fgetl(fhandle));
        rxi = line(1); numcomps2 = line(2); %counts  = line(3); errmsg = line(4);

        for j = 1:numcomps2
            line = str2num(fgetl(fhandle));
            compi = line(1); az = line(2); el = line(3); pwr = line(4); 

            if j > NCOMP
                continue;
            end
            data_rx2(i,j) = rxi;
            data_comp2(i,j) = compi;
            data_el2(i,j) = el;
            data_pwr2(i,j) = pwr;
            data_az2(i,j) = az;
        end
    end
    NaN_index = find(data_pwr2 == 0);
end

fclose(fhandle);
data_phs2(NaN_index) = NaN;
data_rx2(NaN_index) = NaN;
data_comp2(NaN_index) = NaN;
data_pwr2(NaN_index) = NaN;
data_delay2(NaN_index) = NaN;

