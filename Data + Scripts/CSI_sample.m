function [PR, PL_ref, sampling_rate, gride, FFT_grid] = CSI_sample(powers, delays, W, fc, phases, pulses = 256, grid_scale=4, speed_ms=[])
  sampling_rate = round(W * 2);
  doppler_freqs = speed_ms / 3e8 * fc;

  THRE = -40.0;
  PL_ref = max(powers);
  P = powers - PL_ref;

  flt_mask = P > THRE;
  T = delays(flt_mask);
  P = P(flt_mask);
  PH = phases(flt_mask);

  T = T - min(T);

  % Make the quatized time
  pos = round(T * sampling_rate + pulses);

  A = exp(-1j * PH) .* sqrt(10.0.^(P*0.1));

  grid_size = round(pulses * grid_scale);

  if max(T) * sampling_rate > grid_size 0.7
    error("Maximal ToF is too high, get a bigger grid!");
  endif

  pulse_time_axis = linspace(-pulses / 2.0 / sampling_rate, pulses / 2.0 / sampling_rate, pulses);

  if length(doppler_freqs) > 0
    mpc_grids = zeros([len(A), grid_size]);
    for i = 1:1:length(A)
      mpc_grids(pos(i), i) = A(i);
      doppler = doppler_freqs(i);
      Wd = W + doppler;
      pulse = (W / sampling_rate) * sinc(Wd * pulse_time_axis);
      mpc_grids(:, i) = conv(mpc_grids(:, 1), pulse, "same");
    endfor

    gride = sum(mpc_grids, 1);
  else
    gride = zeros([1,grid_size]);

    for i = [1:1:length(pos)]
      gride(pos(i)) = gride(pos(i)) + A(i);
    endfor

    pulse = (W / sampling_rate) * sinc(W * pulse_time_axis);
    gride = conv(gride, pulse, 'same');
  endif

  FFT_grid = fftshift(fft(gride));
  power_corr = 10.0 * log10(sum(abs(gride .* conj(gride))/(W / sampling_rate)));
  PR = PL_ref + power_corr;
endfunction
