function pow_adj = cir_pattern(antpat, cirpw, ciraz, cirel, bw)
  pow_adj = [];
  for i = 1:length(cirpw)
    pw = 10.0^(cirpw(i)/10.0);
    pow_adj = [pow_adj, 10.0 * log10(pw * antpat(int32(ceil(cirel(i)/bw)), int32(ceil(ciraz(i)/bw))))];
  endfor
endfunction
