function [agr, egr, respat] = adjust_pattern(az, elev, beamwidth)
  elevg = [0:deg2rad(beamwidth):pi];
  azg = [-pi:deg2rad(beamwidth):pi];
  [agr,egr] = meshgrid(azg, elevg);
  respat = abs(sinc(agr + deg2rad(az)) .* sinc(egr - deg2rad(elev)));
endfunction
