scenario_name = 'sitting';
scenario_name = 'standing';
poly_scenario_name = 'Detailed';
poly_scenario_name = 'Lowpoly';


THRESHOLD = -100;
MAX_DELAY = 3e-8;
DRAW_PLOT = 1;

folder_name = strcat(scenario_name, '/', poly_scenario_name);
file_name_example = strcat(folder_name,'PDP@[TX0_-_RX60].csv');
        
% switch (scenario_name)
%     case 'sitting'
%         folder_name = strcat(scenario_name, '_subtr20db/');
%         file_name_example = strcat(folder_name,'PDP@[TX0_-_RX60].csv');
%     case 'standing'
%         folder_name = strcat(scenario_name, '/');
%         file_name_example = strcat(folder_name,'PDP@[TX0_-_RX60].csv');
% end


%M = csvread(filename)
file_name = file_name_example;
listing = dir(folder_name);


N_bins = 1000;
bin_array = 0:MAX_DELAY/N_bins:MAX_DELAY;
delta_bin = MAX_DELAY/N_bins;

sum_bin_array_y = zeros(size(bin_array));
delay_tap_array = [];
% for i = 1:length(delay_array_filtered)
%         bin_array_x(i) = floor(delay_array_filtered(i)/delta_bin);
%         bin_array_y(i) = bin_array_y(i) + power_array_filtered(i);
% end
    
    
for j = 3:length(listing)
    file_name = listing(j).name;
    file_name = strcat(folder_name, file_name);
% end

    raw_data = csvread(file_name,1,0);
    data_sample_size = length(raw_data);

    [delay_array, index] = sort(raw_data(:,1));
    power_array = raw_data(index,2);
    delay_array = delay_array - delay_array(1);

    if power_delta_db
        power_array = power_array + power_delta_db;
    end


    bin_frequency = zeros(size(bin_array));
    bin_array_y = zeros(size(bin_array));
    
    index = find(power_array > THRESHOLD);
    power_array_filtered = power_array(index);
    delay_array_filtered = delay_array(index);
    
    delay_tap_array = [delay_tap_array;delay_array_filtered(2:length(delay_array_filtered))];
    
    for i = 1:length(delay_array_filtered)
        n = floor(delay_array_filtered(i)/delta_bin) + 1;
        if n <= length(bin_array_y)
            bin_array_y(n) = bin_array_y(n) + power_array_filtered(i);
           
            bin_frequency(n) = bin_frequency(n) + 1;
        else
            stop = 1;
        end
        if power_array_filtered(i) > -30
            stop = 1;
        end
    end
    %bin_array_y = 10*log10(bin_array_y);
    
    
    sum_bin_array_y = sum_bin_array_y + bin_array_y./max(1,bin_frequency);
    if 0%DRAW_PLOT
        figure;hist(delay_array_filtered(2:length(delay_array_filtered)),20)

        
        
        figure;
        index = find(bin_array_y<0)
        stem(bin_array(index),bin_array_y(index)./bin_frequency(index), 'filled', 'BaseValue', -120)

        figure;
         stem(delay_array, power_array, 'filled', 'BaseValue', -120); hold on
         plot([delay_array(1), delay_array(data_sample_size)], THRESHOLD*ones(1,2));
        
         figure;title(scenario_name);hold on;
         stem(delay_array_filtered, power_array_filtered, 'filled', 'BaseValue', THRESHOLD); hold on
     
        stop = 0;
%         figure;
%         plot(bin_array_x, bin_array_y);
    end
    figure;title(scenario_name);hold on;xlim([0, 3e-9])
         stem(delay_array_filtered, power_array_filtered, 'filled', 'BaseValue', THRESHOLD); hold on
     
   
end

figure;hist(delay_tap_array,100)

figure;
index = find(sum_bin_array_y<0)
stem(bin_array(index),sum_bin_array_y(index)./(length(listing)-2), 'filled', 'BaseValue', -120);%/(length(listing) - 2))
%min(bin_frequency,1)
stop = 0;



% stem(delay_array_filtered, 10.^(power_array_filtered/10), 'filled', 'BaseValue', 0); hold on

% [xData, yData] = prepareCurveData( delay_array_filtered, power_array_filtered );
% ft = 'linearinterp';
% [fitresult, gof] = fit( xData, yData, ft, 'Normalize', 'on' );
% for i = 1:length(fitresult.p.coefs)
%     x(2*i+1) = xData(i);
%     x(2*i+2) = xData(i+1);
%     y(2*i+1) = power_array_filtered(i);
%     y(2*i+2) = power_array_filtered(i+1);
% end
% figure;
% plot(x,y)
