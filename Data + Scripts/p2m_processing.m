clear all;
close all;
clc;

scenario_name = 'Sitting';
scenario_name = 'Standing';
poly_scenario_name = 'Human_crawl';
data = 'cir'; % Allowed values 'dod', 'doa' and 'cir'
NCOMP = 25; % Number of components to plot
RXGRP = 2; % Receiver group [2 - Belt, 3 - Upwards, 5 - Diagonal, 6 - Head]
cm = cool;

folder_name = strcat('OLD/',scenario_name, '/');
file_name_example = strcat(folder_name,sprintf(strcat(poly_scenario_name, '.', data, '.', 't%03d_01.r%03d.p2m'), 1, RXGRP));
        
fhandle = fopen(file_name_example);

if data == 'cir'
  fskipl(fhandle, 2);
  strread
else
  fskipl(fhandle, 1);
end
  

line = fgetl(fhandle);
[numpoints, counts, errmsg] = sscanf(line, ' %d', 'C');

if counts ~= 1
  error('Failed to read!');
end

dataX = [];
dataC = [];

if data == 'cir'
  data_pwr = [];
  data_phs = [];
  data_delay = [];
else
  data_az = [];
  data_el = [];
  data_pwr = [];
end

for i = 1:numpoints
  line = fgetl(fhandle);
  [rxi, numcomps, counts, errmsg] = sscanf(line, '%d %d', 'C');
 
  if counts ~= 2
    error('Failed to read!');
  end
 
  for j = 1:numcomps
    line = fgetl(fhandle);
    if data == 'cir'
      [compi, phs, delay, pwr, counts, errmsg] = sscanf(line, '%d %f %f %f', 'C');
    else
      [compi, az, el, pwr, counts, errmsg] = sscanf(line, '%d %f %f %f', 'C');
    end
    
    
    if j > NCOMP
      continue;
    end
    
    if counts ~= 4
      error('Failed to read!');
    end
    
    dataC = [dataC, NCOMP - j];
    dataX = [dataX, i];
    
    if data == 'cir'
      data_phs = [data_phs, phs];
      data_pwr = [data_pwr, pwr];
      data_delay = [data_delay, delay];
    else
      data_el = [data_el, el];
      data_az = [data_az, az];
      data_pwr = [data_pwr, pwr];
    end
  end
end

fclose(fhandle);



if data == 'cir'
  figure();
  scatter(dataX, data_phs, [], dataC);colormap(cm);
  xlabel('RX index');
  ylabel('Phase [deg]');
  grid on;
  figure();
  scatter(dataX, data_pwr, [], dataC);colormap(cm);
  xlabel('RX index');
  ylabel('Power [dBm]');
  grid on;
  figure();
  scatter(dataX, data_delay*1e9, [], dataC);colormap(cm);
  xlabel('RX index');
  ylabel('Delay [ns]');
  grid on;
else
  figure();
  scatter(dataX, data_el, [], dataC);colormap(cm);
  xlabel('RX index');
  ylabel('Elevation [deg]');
  ylim([0,180]);
  grid on;
  figure();
  scatter(dataX, data_az, [], dataC);colormap(cm);
  xlabel('RX index');
  ylabel('Azimuth [dBm]');
  grid on;
  figure();
  scatter(dataX, data_pwr*1e9, [], dataC);colormap(cm);
  xlabel('RX index');
  ylabel('Power [dBm]');
  grid on;
end;
