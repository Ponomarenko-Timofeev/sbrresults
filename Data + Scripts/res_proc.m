
%% Standing
load LOS-pen__standing_fleece_regr.mat
LOS_pen_standing_fleece_res = residues;
LOS_pen_standing_fleece_xdata_raw = xdata_raw;
LOS_pen_standing_fleece_ydata_raw = ydata_raw;
figure;plot(10.^LOS_pen_standing_fleece_xdata_raw, LOS_pen_standing_fleece_ydata_raw,'b.')
figure;hist(LOS_pen_standing_fleece_res,40)
mean(LOS_pen_standing_fleece_res)

LOS_pen_all_fleece_res = [LOS_pen_sitting_fleece_res,LOS_pen_standing_fleece_res];
figure;hist(LOS_pen_all_fleece_res,40)
mean(LOS_pen_all_fleece_res)

LOS_pen_all_fleece_res_cut = LOS_pen_all_fleece_res(find(LOS_pen_all_fleece_res<1))
figure;hist(LOS_pen_all_fleece_res_cut,40)
mean(LOS_pen_all_fleece_res_cut)


load LOS-pen__standing_regr.mat
LOS_pen_standing_res = residues;
LOS_pen_standing_xdata_raw = xdata_raw;
LOS_pen_standing_ydata_raw = ydata_raw;
figure;plot(10.^LOS_pen_standing_xdata_raw, LOS_pen_standing_ydata_raw,'b.')
figure;hist(LOS_pen_standing_res,40)
mean(LOS_pen_standing_res)


load LOS-pen__standing_leather_regr.mat
LOS_pen_standing_leather_res = residues;
LOS_pen_standing_leather_xdata_raw = xdata_raw;
LOS_pen_standing_leather_ydata_raw = ydata_raw;
figure;plot(10.^LOS_pen_standing_leather_xdata_raw, LOS_pen_standing_leather_ydata_raw,'g.')
figure;hist(LOS_pen_standing_leather_res,40)
mean(LOS_pen_standing_leather_res)




%% Sitting
load LOS-pen__sitting_fleece_regr.mat
LOS_pen_sitting_fleece_res = residues;
LOS_pen_sitting_fleece_xdata_raw = xdata_raw;
LOS_pen_sitting_fleece_ydata_raw = ydata_raw;
figure;plot(10.^LOS_pen_sitting_fleece_xdata_raw, LOS_pen_sitting_fleece_ydata_raw,'b.')
figure;hist(LOS_pen_sitting_fleece_res,40)
mean(LOS_pen_sitting_fleece_res)

load LOS-pen__sitting_leather_regr.mat
LOS_pen_sitting_leather_res = residues;
LOS_pen_sitting_leather_xdata_raw = xdata_raw;
LOS_pen_sitting_leather_ydata_raw = ydata_raw;
figure;plot(10.^LOS_pen_sitting_leather_xdata_raw, LOS_pen_sitting_leather_ydata_raw,'g.')
figure;hist(LOS_pen_sitting_leather_res,40)
mean(LOS_pen_sitting_leather_res)

load LOS-pen__sitting_regr.mat
LOS_pen_sitting_res = residues;
LOS_pen_sitting_xdata_raw = xdata_raw;
LOS_pen_sitting_ydata_raw = ydata_raw;
figure;plot(10.^LOS_pen_sitting_xdata_raw, LOS_pen_sitting_ydata_raw,'m.')
figure;hist(LOS_pen_sitting_res,40)
mean(LOS_pen_sitting_res)




%% NLOS Sitting
load NLOS-pen__sitting_fleece_regr.mat
NLOS_pen_sitting_fleece_res = residues;
NLOS_pen_sitting_fleece_xdata_raw = xdata_raw;
NLOS_pen_sitting_fleece_ydata_raw = ydata_raw;
figure;plot(10.^NLOS_pen_sitting_fleece_xdata_raw, NLOS_pen_sitting_fleece_ydata_raw,'b.')
figure;hist(NLOS_pen_sitting_fleece_res,40)
mean(NLOS_pen_sitting_fleece_res)

load NLOS-pen__sitting_leather_regr.mat
NLOS_pen_sitting_leather_res = residues;
NLOS_pen_sitting_leather_xdata_raw = xdata_raw;
NLOS_pen_sitting_leather_ydata_raw = ydata_raw;
figure;plot(10.^NLOS_pen_sitting_leather_xdata_raw, NLOS_pen_sitting_leather_ydata_raw,'g.')
figure;hist(NLOS_pen_sitting_leather_res,40)
mean(NLOS_pen_sitting_leather_res)

load NLOS-pen__sitting_regr.mat
NLOS_pen_sitting_res = residues;
NLOS_pen_sitting_xdata_raw = xdata_raw;
NLOS_pen_sitting_ydata_raw = ydata_raw;
figure;plot(10.^NLOS_pen_sitting_xdata_raw, NLOS_pen_sitting_ydata_raw,'m.')
figure;hist(NLOS_pen_sitting_res,40)
mean(NLOS_pen_sitting_res)
mean(NLOS_pen_sitting_ydata_raw)
mean(NLOS_pen_sitting_leather_ydata_raw)
mean(NLOS_pen_sitting_fleece_ydata_raw)




%% NLOS Sitting
load NLOS-pen__standing_fleece_regr.mat
NLOS_pen_standing_fleece_res = residues;
NLOS_pen_standing_fleece_xdata_raw = xdata_raw;
NLOS_pen_standing_fleece_ydata_raw = ydata_raw;
figure;plot(10.^NLOS_pen_standing_fleece_xdata_raw, NLOS_pen_standing_fleece_ydata_raw,'b.')
%figure;plot(10.^NLOS_pen_standing_fleece_xdata_raw*100, NLOS_pen_standing_fleece_ydata_raw,'b.')
figure;hist(NLOS_pen_standing_fleece_res,40)
mean(NLOS_pen_standing_fleece_res)

load NLOS-pen__standing_leather_regr.mat
NLOS_pen_standing_leather_res = residues;
NLOS_pen_standing_leather_xdata_raw = xdata_raw;
NLOS_pen_standing_leather_ydata_raw = ydata_raw;
figure;plot(10.^NLOS_pen_standing_leather_xdata_raw, NLOS_pen_standing_leather_ydata_raw,'g.')
%figure;plot(10.^NLOS_pen_standing_leather_xdata_raw*100, NLOS_pen_standing_leather_ydata_raw,'g.')

figure;hist(NLOS_pen_standing_leather_res,40)
mean(NLOS_pen_standing_leather_res)

load NLOS-pen__standing_regr.mat
NLOS_pen_standing_res = residues;
NLOS_pen_standing_xdata_raw = xdata_raw;
NLOS_pen_standing_ydata_raw = ydata_raw;
figure;plot(10.^NLOS_pen_standing_xdata_raw, NLOS_pen_standing_ydata_raw,'m.')
figure;hist(NLOS_pen_standing_res,40)
mean(NLOS_pen_standing_res)
mean(NLOS_pen_standing_ydata_raw)
mean(NLOS_pen_standing_leather_ydata_raw)
mean(NLOS_pen_standing_fleece_ydata_raw)



figure;plot(10.^NLOS_pen_standing_xdata_raw*100, NLOS_pen_standing_ydata_raw,'m.')