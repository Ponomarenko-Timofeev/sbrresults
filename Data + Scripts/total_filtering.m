% Complete channel image saver

clear all;
close all;
clc;

%pkg load pythonic;
pkg load miscellaneous;

set(0, "defaultaxesfontsize", 20);
set(0, "defaulttextfontsize", 20);
%pyversion
%py.sys.path.insert(int32(0), './');

DATABASE = 3;
RXGRP = 4; % Receiver group [2 - Belt, 4 - Upwards, 5 - Diagonal, 6 - Head]
%scenario_name = 'Sitting';
scenario_name = 'Standing';

NF = -130; % Noise floor of -130 dBm
MAXPOW = -20;
ang_step = 1.0; % 1 degree step

ele_range = linspace(0.0, 180.0, 180.0/ang_step);
az_range = linspace(0.0, 360.0, 360.0/ang_step);

[ele, az] = meshgrid(ele_range, az_range);

GR_AZ = az;
GR_EL = ele;

arrpow = ones(size(ele)) * NF;
delay = zeros(size(ele));
phase = zeros(size(ele));
deppow = ones(size(ele)) * NF;



imwrite(arrpow, "Power.png");
imwrite(delay, "Delay.png");
imwrite(phase, "Phase.png");
imwrite(deppow, "Deppow.png");

ANT_GAIN = abs(sinc(GR_AZ) .* sinc(GR_EL));

NCOMP = 50; % Number of components to plot
cm = cool;
switch DATABASE
    case 4
        %UNCLEAR
        poly_scenario_name = 'Human_sitting_legsback';
        folder_name = strcat('../Grid_antenna/');
        tail = 't%03d_01.r%03d.p2m';
    case 1
        %scenario_name = 'Standing';
        poly_scenario_name = 'Human_sitting_legsback';
        %RXGRP = 4; % Receiver group [2 - Belt, 3 - Upwards, 5 - Diagonal, 6 - Head]
        %folder_name = strcat('OLD/',scenario_name, '/');
        folder_name = strcat(scenario_name, '/Detailed/');
        tail = 't%03d_01.r%03d.p2m';scenario_name = 'sitting';

    case 2 %WORKS
        poly_scenario_name = 'Human_sitting_legsback';
        folder_name = strcat(scenario_name, '/Lowpoly/');
        tail = 't%03d_01.r%03d.p2m';
    case 3 % THE ONE!!
        switch scenario_name
            case 'Sitting'
                poly_scenario_name = 'HMS_lp2_sitting';
            case 'Standing'
                poly_scenario_name = 'HMS_lp2';
            otherwise
                display('Error: unknown scenario!');
            return
        end
        tail = 't%03d_01.r%03d.p2m';
        if RXGRP == 4
            RXGRP = 3;
        end
        folder_name = strcat('OLD/',scenario_name, '/');
    otherwise
        display('Error: unknown scenario!');
        return
end
file_name_example = strcat(folder_name,sprintf(strcat(poly_scenario_name, '.xxx.', 't%03d_01.r%03d.p2m'), 1, RXGRP));
display(strcat('I will read <', file_name_example,'>'))
%scenario_name = 'Standing';poly_scenario_name = 'Human_sitting_legsback';
%poly_scenario_name = 'Human_crawl';




%% READ DATA
data =  'cir';
[data_rx2, data_comp2, data_pwr2, data_phs2, data_delay2, temp1, temp2, error1] = ...
    Read_WI_file(folder_name, poly_scenario_name, tail, 'cir', RXGRP, NCOMP);
[data_rx2_dod, data_comp2_dod, data_pwr2_dod, temp4, temp3, data_az2_dod, data_el2_dod, error2] = ...
    Read_WI_file(folder_name, poly_scenario_name, tail, 'dod', RXGRP, NCOMP);     
[data_rx2_doa, data_comp2_doa, data_pwr2_doa, temp4, temp3, data_az2_doa, data_el2_doa, error3] = ...
    Read_WI_file(folder_name, poly_scenario_name, tail, 'doa', RXGRP, NCOMP);      
if error1 || error2 || error3
    display('Error: cannot open the file!');
    return
end
if    ~isequaln(data_rx2_dod,data_rx2_doa) || ~isequaln(data_rx2,data_rx2_dod)...
        ~isequaln(data_comp2,data_comp2_doa) || ~isequaln(data_comp2,data_comp2_dod)
        display('Error: different data sets!');
        return
end
if    ~isequaln(data_pwr2,data_pwr2_doa) || ~isequaln(data_pwr2,data_pwr2_dod)
        display('Error: different data sets!');
        return
end


color_scheme(1,1:3) = [0 0 0]; color_scheme2(1,1:3) = [0 0 0];
color_max = [1 0.5 1];
delta_color = (color_max - color_scheme(1,1:3))/(NCOMP - 1);
delta_color2 = (color_max - color_scheme2(1,1:3))/(size(data_pwr2,2) - 1);
for i = 2:size(data_pwr2,1)
  color_scheme(i,1:3) = color_scheme(i-1,1:3) + delta_color;
end
for i = 2:size(data_pwr2,2)
  color_scheme2(i,1:3) = color_scheme2(i-1,1:3) + delta_color2;
end
 
P = [];
fftwf = [];

%for i = 1:size(data_pwr2, 1)
%  azconf = data_az2_doa(i,1);
%  elconf = data_el2_doa(i,1);
%  [az,el,pat] = adjust_pattern(azconf, elconf, 10);
%  f = figure("visible","off");
%  surf(az,el,pat);
%  title(sprintf("RX pos %04d", i));
%  xlabel("Azimuth [rad]");
%  ylabel("Elevation [rad]");
%  zlabel("Gain [linear]");
%  set (findobj (gcf, "-property", "interpreter"), "interpreter", "latex");
%  print(sprintf("Dpat_rx_%04d.tex", i), '-dtex', "-F:24");
%  close(f);
%  powadj = cir_pattern(pat, data_pwr2(i,:), (data_az2_doa(i,:)) + 180.0, data_el2_doa(i,:), 10);
%  [PL, PLr, gridd, fftgrid] = get_CSI(powadj, data_delay2(i,:), 80e6, 60e9, data_phs2(i,:));
%  P = [P, PL];
%  fftwf = [fftwf; fftgrid];
%  i
%endfor

P2 = [];
fftwf2 = [];

az_thr = 10;
el_thr = 10;

%for k = 1:1:length(data_delay2)
%  d = data_delay2(k,:);
%  p = data_pwr2(k,:);
%  ph = data_phs2(k,:);
%  az = data_az2_doa(k,1);
%  el = data_el2_doa(k,1);
  
%  mask = ((az+az_thr) > data_az2_doa(k, :)) .* (data_az2_doa(k, :) > (az-az_thr)) .* ...
%         ((el+el_thr) > data_el2_doa(k, :)) .* (data_el2_doa(k, :) > (el-el_thr));
  
%  [PL, PLr, gridd, fftgrid] = get_CSI(p(mask == 1), d(mask == 1), 80e6, 60e9, ph(mask == 1));
%  P2 = [P2, PL];
%  fftwf2 = [fftwf2; fftgrid];
%end

%P3 = [];
%fftwf3 = [];

%for k = 1:1:length(data_delay2)
%  d = data_delay2(k,:);
%  p = data_pwr2(k,:);
%  ph = data_phs2(k,:);
%  [PL, PLr, gridd, fftgrid] = get_CSI(p, d, 80e6, 60e9, ph);
%  P3 = [P3, PL];
%  fftwf3 = [fftwf3; fftgrid];
%end

%figure;
%hold on;
%grid on;
%plot(1:1:length(P2), P2, 'linewidth', 2);
%plot(1:1:length(P3), P3, 'linewidth', 2);
%plot(1:1:length(data_pwr2(:,1)), data_pwr2(:,1), 'linewidth', 2);
%ylim([-100,-40]);
%xlim([0,120]);
%xlabel("Receiver index");
%ylabel("Received power [dBm]");
%legend("Sector filtering", "Total power of 10 components", "First component", "location", "southwest");
%set (findobj (gcf, "-property", "interpreter"), "interpreter", "tex")
%print(sprintf("RXpow_filt_10.tex", i), '-dtex', "-F:24");

%P4 = [];
%fftwf4 = [];
 
%for i = 1:size(data_pwr2, 1)
%  azconf = data_az2_dod(i,1);
%  elconf = data_el2_dod(i,1);
%  [az,el,pat] = adjust_pattern(azconf, elconf, 10);
  %f = figure("visible","off");
  %surf(az,el,pat);
  %title(sprintf("RX pos %04d", i));
  %xlabel("Azimuth [rad]");
  %ylabel("Elevation [rad]");
  %zlabel("Gain [linear]");
  %set (findobj (gcf, "-property", "interpreter"), "interpreter", "latex");
  %print(sprintf("Dpat_tx_%04d.tex", i), '-dtex', "-F:24");
  %close(f);
%  powadj = cir_pattern(pat, data_pwr2(i,:), (data_az2_dod(i,:)) + 180.0, data_el2_dod(i,:), 10);
%  [PL, PLr, gridd, fftgrid] = get_CSI(powadj, data_delay2(i,:), 80e6, 60e9, data_phs2(i,:));
 % P4 = [P4, PL];
 % fftwf4 = [fftwf4; fftgrid];
 % i
%endfor

# P5 = [];
# fftwf5 = [];
# 
# az_thr = 10;
# el_thr = 10;
# 
# for k = 1:1:length(data_delay2)
#   d = data_delay2(k,:);
#   p = data_pwr2(k,:);
#   ph = data_phs2(k,:);
#   az = data_az2_dod(k,1);
#   el = data_el2_dod(k,1);
#   
#   mask = ((az+az_thr) > data_az2_dod(k, :)) .* (data_az2_dod(k, :) > (az-az_thr)) .* ...
#          ((el+el_thr) > data_el2_dod(k, :)) .* (data_el2_dod(k, :) > (el-el_thr));
#   
#   [PL, PLr, gridd, fftgrid] = CSI_sample(p(mask == 1), d(mask == 1), 80e6, 60e9, ph(mask == 1));
#   P5 = [P5, PL];
#   fftwf5 = [fftwf5; fftgrid];
# end
# 
# P5 = [];
# fftwf5 = [];
# 
# az_thr = 10;
# el_thr = 10;
# 
# for k = 1:1:length(data_delay2)
#   d = data_delay2(k,:);
#   p = data_pwr2(k,:);
#   ph = data_phs2(k,:);
#   az = data_az2_dod(k,1);
#   el = data_el2_dod(k,1);
#   
#   mask = ((az+az_thr) > data_az2_dod(k, :)) .* (data_az2_dod(k, :) > (az-az_thr)) .* ...
#          ((el+el_thr) > data_el2_dod(k, :)) .* (data_el2_dod(k, :) > (el-el_thr));
#   
#   [PL, PLr, gridd, fftgrid] = CSI_sample(p(mask == 1), d(mask == 1), 80e6, 60e9, ph(mask == 1));
#   P5 = [P5, PL];
#   fftwf5 = [fftwf5; fftgrid];
# end

%figure;
%hold on;
%grid on;
%plot(1:1:length(P2), P2, 'linewidth', 2);
%plot(1:1:length(Pang), Pang, 'linewidth', 2);
%plot(1:1:length(P3), P3, 'linewidth', 2);
%plot(1:1:length(P5), P5, 'linewidth', 2);
%ylim([-100,-40]);
%xlim([0,120]);
%xlabel("Receiver index");
%ylabel("Received power [dBm]");
%hold on; plot([0,length(Pang)], [data_pwr2(poi,1), data_pwr2(poi,1)], 'linewidth', 2);
%legend("RX filtering", "Total power", "TX filtering", "location", "southwest");
%print(sprintf("Unrealcut.tex", i), '-dtex', "-F:24");


k=86;
PtxGT = [];
PrxGT = [];
offsets = 10:1:60;
for offset = offsets
  PtxG = [];
  secsizes = 20:1:60;
  for i = secsizes
    text_waitbar((((find(offsets==offset) - 1)*length(secsizes)+(find(i==secsizes) - 1))/(length(secsizes)*length(offsets))));
    gain = 10*log10(2/(1 - cos(deg2rad(i)/2)));
  
  d = data_delay2(k,:);
  p = data_pwr2(k,:);
  ph = data_phs2(k,:);
  az = data_az2_dod(k,1)+offset;
  el = data_el2_dod(k,1);
  
  mask = ((az+i) > data_az2_dod(k, :)) .* (data_az2_dod(k, :) > (az-i)) .* ...
         ((el+i) > data_el2_dod(k, :)) .* (data_el2_dod(k, :) > (el-i));
  
  p = p + gain;
  
  if sum(mask) > 0 
  	[PL, PLr, gridd, fftgrid] = CSI_sample(p(mask == 1), d(mask == 1), 80e6, 60e9, ph(mask == 1));
  else
	PL= 0;
  endif

  PtxG = [PtxG, PL];
endfor


PrxG = [];
for i = secsizes
  gain = 10*log10(2/(1 - cos(deg2rad(i)/2)));
  
  d = data_delay2(k,:);
  p = data_pwr2(k,:);
  ph = data_phs2(k,:);
  az = data_az2_doa(k,1)+offset;
  el = data_el2_doa(k,1);
  
  mask = ((az+i) > data_az2_doa(k, :)) .* (data_az2_doa(k, :) > (az-i)) .* ...
         ((el+i) > data_el2_doa(k, :)) .* (data_el2_doa(k, :) > (el-i));
  
  p = p + gain;
  if sum(mask) > 0 
  	[PL, PLr, gridd, fftgrid] = CSI_sample(p(mask == 1), d(mask == 1), 80e6, 60e9, ph(mask == 1));
  else
	PL= 0;
  endif

  PrxG = [PrxG, PL];
endfor

PtxGT = [PtxGT; PtxG];
PrxGT = [PrxGT; PrxG];
endfor

PtxGT(PtxGT == 0) = min(min(PtxGT));
PrxGT(PrxGT == 0) = min(min(PrxGT));

figure;
colormap("copper");
hold on;
grid on;
[X,Y] = meshgrid(1:1:size(PrxGT,2), 1:1:size(PrxGT,1));
surf(X+20,Y+10,PtxGT);
%plot(secsizes, PrxG, "linewidth", 2);
xlabel("Half beamwidth [deg]");
zlabel("Received power [dBm]");
ylabel("Beam misalignment [deg]");
title("RX filtering");
view(275,30);
print("TXT.svg", "-dsvg", "-FArial:24");
box on;
hold off;

figure;
colormap("copper");
hold on;
grid on;
[X,Y] = meshgrid(1:1:size(PrxGT,2), 1:1:size(PrxGT,1));
surf(X+20,Y+10,PrxGT);
%plot(secsizes, PrxG, "linewidth", 2);
xlabel("Half beamwidth [deg]");
zlabel("Received power [dBm]");
ylabel("Beam misalignment [deg]");
title("TX filtering");
view(275,30);
print("RXT.svg", "-dsvg", "-FArial:24");
box on;
hold off;
