## Wireless Insite SBR Simulation Results for On-Body Scenarios

This repository contains post-processing scripts and simulation results for the IEEE ICC sork we submitted.


## System requirements

Octave with miscellaneous package installed, if you have octave running, just issue the following command:

~~~
pkg install -forge miscellaneous
~~~

Since the scripts are cross-compatible between different systems, there should be no severe issue while running them. However, I recommend to use any Linux distro, since the environment is much easier to set up. Scripts are not optimized as of now, so performance may be an issue on weaker machines. I use a machine with i7-2600 cPU and 12 GB of RAM to run the scripts (with other tasks running in background).



## Overall workflow

The scripts load the data from the OLD folder, picking either Sitting or Standing scenario based on what is selected in the script.


## Scripts

The main scripts for producing the plots in the paper are:

 * total_filtration.m - used to build Fig. 8 from the paper, demonstrating how the antenna pattern affects received power. You can tweak the parameters (trajectory/number of components taken into account/offset and angle of filtration)

 * p2m_ncomp_effect.m - Used to generate a set of plots given on Fig. 3. This script must be ran two times for each of the scenarios (sitting/standing) to generate complete plots. Do NOT close the plots as the script plots over the old ones.

 * p2m_characteristics.m - Used to build Figs. 5,6 and 7.

Apart from them, a number of auxiliary scripts are present (reading data, calculating received power with regard for phases, etc.)
 

## FAQ

 * The Fig. 8 differs from one shown in the paper, why?
 :  Figure 8 was obtained by applying a rotation view transform to the plot. Initially it is projected with top-down perspective.
 * Some of the plots have different color scheme/fonts.
 :  We post-processed the plots, changed colormaps and fonts, so that explains it.
 * The scripts are slow and consume a lot of RAM
 :  I am not that familiar with Octave and did not have much time to optimize the scripts. Optimizing them was not the primary task at all during the study.
 * Why Octave?
 : Because MATLAB eats a lot of RAM and would be an overkill for the task.
